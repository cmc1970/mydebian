FROM debian:stable-slim

ARG DEBIAN_FRONTEND=noninteractive

RUN \
    apt update && apt install -yq  \
    systemd \
    init \
    linux-image-amd64 \
    grub-pc-bin \
    grub2-common \
    iproute2 \
    pciutils \
    mc \
    acl \
    iputils-ping \
    less \
    nano \
    xz-utils \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common \
    xserver-xorg-video-dummy \
    xserver-xorg-input-void \
    xserver-xorg-core \
    xinit \
    x11-xserver-utils \
    xserver-xorg-input-mouse \
    xserver-xorg-input-kbd \
    xserver-xorg-input-libinput \
    spectrwm \
    scrot \
    && apt clean
